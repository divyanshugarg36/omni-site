import React, { useState } from 'react';
import Swal from 'sweetalert2'
// import * as styles from './EmailListForm.module.scss';
import addToMailchimp from 'gatsby-plugin-mailchimp';

const EmailListForm = () => {

  const [email, setEmail] = useState('');

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  // };
  const handleSubmit = (e) => {
    e.preventDefault();

    addToMailchimp(email)
      .then((data) => {
        // console.log(data);
        // alert(data.result);
        if (data.result == "success") {
          return Swal.fire({
            icon: 'success',
            title: "Great!",
            text: "You've been subscribed successfully",
          })
        }
        let message = data.msg || 'Something went wrong!'
        return Swal.fire({
          icon: 'error',
          title: 'Oops...',
          html: message,
        })
      })
      .catch((error) => {
        // Errors in here are client side
        // Mailchimp always returns a 200
        console.log("error");
      });
  };

  const handleEmailChange = (event) => {
    setEmail(event.currentTarget.value);
  };

  return (
    <form onSubmit={handleSubmit} id="emailbox-form">
      <h2 id="email-description">Get the notification when new article released</h2>
      <div id="email-box">
        <input
          placeholder="Enter email"
          name="email"
          type="text"
          onChange={handleEmailChange}
          id="email-input"
          required
        />
        <button type="submit" id="email-submit">Submit</button>
      </div>
    </form>
  );
};

export default EmailListForm;