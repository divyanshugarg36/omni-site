import { Link } from "gatsby"
// import PropTypes from "prop-types"
import React from "react"
// import '../scss/style.scss'

const smoothScroll = (element) => {
  document.querySelector('.menu-icon').click();
  const menuItem = document.querySelector(`a[href="/${element}"]`);
  
  const prevMenu = document.querySelector('.active-menu-item');
  if(prevMenu) prevMenu.classList.remove('active-menu-item');

  menuItem.classList.add('active-menu-item');
  console.log(menuItem);
  window.scrollTo({
    top: document.querySelector(element).offsetTop,
    left: 0,
    behavior: 'smooth'
  });
}


const Header = () => (
  <div class="top-bar animated slideInDown">
    <div class="nav">
      <div class="branding">
          <img src="/images/logo.svg" alt="logo" />
          <img src="/images/omni-text.svg" class="text-logo" alt="logo-text" />
      </div>
      <div class="menu-icon" onClick={(e) => {
        const icon = e.currentTarget;
        icon.classList.toggle('open');
        document.querySelector('.menu').classList.toggle('open');
        document.querySelector('body').classList.toggle('open-menu');
      }}>
        <span class="top"></span>
        <span class="middle"></span>
        <span class="bottom"></span>
      </div>
      <div class="menu">
        <ul>
          <li>
            <Link to="#about" onClick={ e => {
              e.preventDefault();
              smoothScroll('#about');
             }}>
              About
            </Link>
          </li>
          <li>
            <Link to="#products" onClick={ e => {
              e.preventDefault();
              smoothScroll('#products');
             }}>
              Products
            </Link>
          </li>
          <li>
            <Link to="#projects" onClick={ e => {
              e.preventDefault();
              smoothScroll('#projects');
             }}>
              Projects
            </Link>
          </li>
          <li>
            <Link to="#testimonials" onClick={ e => {
              e.preventDefault();
              smoothScroll('#testimonials');
             }}>
              Testimonials
            </Link>
          </li>
          <li>
            <Link to="#team" onClick={ e => {
              e.preventDefault();
              smoothScroll('#team');
             }}>
              Team
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </div>
)



// Header.propTypes = {
//   // siteTitle: PropTypes.string,
// }

// Header.defaultProps = {
//   siteTitle: ``,
// }

export default Header
