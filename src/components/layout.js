import React, { useEffect } from "react"
import PropTypes from "prop-types"

import Header from "./header"
import "../scss/style.scss"
import AOS from 'aos';
import 'aos/dist/aos.css';

const Layout = ({ children }) => {
  useEffect(() => {
    const AOS = require("aos");
    AOS.init({
      duration: 600,
      easing: 'ease-in-sine',
    });
  }, []);

  useEffect(() => {
    if (AOS) {
      AOS.refresh();
    }
  });
  return (
    <>
      <div class="page-wrapper">
        <Header />
        <main>{children}</main>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
