import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

export const FeaturedPostCard = ({ path, title, cover, author_name }) => {
  console.log("author in featured:", author_name);
  return (
    <>
    <Link to={path}>
      <div class="featured-post-card">
        
        <div class="image" style={{backgroundImage: 'url(' + cover.src + ')'}}>
        </div>
        
        <div class="featured-caption-box">
          <p class="author">By {author_name}</p>
          <p class="title">{title}</p>
        </div>
      </div>
    </Link>
    </>
  );
}

export const BlogPostCard = ({ path, title, cover, author_name }) => {
  return (
    <>
    <Link to={path}>
      <div class="post-card">
        <div class="image" style={{backgroundImage: 'url(' + cover.src + ')'}}>
        </div>
        <div class="caption-box">
          <p class="author">By {author_name}</p>
          <p class="title">{title}</p>
        </div>
      </div>
    </Link>
    </>
  );
}

export default BlogPostCard;

BlogPostCard.propTypes = {
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  cover: PropTypes.object,
  author_name: PropTypes.string.isRequired,
};

FeaturedPostCard.propTypes = {
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  cover: PropTypes.object,
  author_name: PropTypes.string.isRequired,
};
