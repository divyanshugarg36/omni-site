import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { BlogPostCard, FeaturedPostCard } from "../components/PostList"
import EmailListForm from "../components/emailBox"
import chunk from "lodash/chunk"

class Blog extends React.Component {
  constructor(props) {
    super(props);
    let postsToShow = 4
    // if (typeof window !== `undefined`) {
    //   postsToShow = window.postsToShow
    //   console.log("poststoshow in if:", postsToShow);
    // }
    let totalPosts = props.data.normalPostsQuery.edges.length - 1;
    this.state = {
      postsToShow,
      showingMore: postsToShow <= totalPosts,
      allPosts: totalPosts
    }

  }
  
  render(props) {
    console.log("data:", this.props);
    const {data} = this.props;

  
    const availableFeaturedPost = data.featuredPostQuery.edges;
    let featuredPost = {};
    if (availableFeaturedPost.length) {
      featuredPost = availableFeaturedPost[0].node;
    }
    const {normalPostsQuery: normalPosts} = data;
    console.log("postsToShow:", this.state);
    console.log("all posts:", normalPosts.edges.length);
    // featuredPost = this.featuredPost;
    // let counter = 0;
    return (
      <Layout>
        <SEO title="Blog Page" />
        <div class="blog">
          <h1 class="blog-title">Blog</h1>
          <div class="all-posts">
            <div class="featured">{
              featuredPost.frontmatter ? (
                <FeaturedPostCard
                  path={featuredPost.frontmatter.path}
                  title={featuredPost.frontmatter.title}
                  date={featuredPost.frontmatter.date}
                  cover={featuredPost.frontmatter.cover.childImageSharp.fluid}
                  featured={featuredPost.frontmatter.featured}
                  author_name={featuredPost.frontmatter.author_name}
                />) : ''
              }
            </div>
            <div class="posts">
              {/* {normalPosts.edges.map(post =>{ */}
                {chunk(normalPosts.edges.slice(0, this.state.postsToShow), 6).map((chunk, i) => {

                  return (
                    <div class="chunk" key={`chunk-${i}`}>
                      {chunk.map((post, i) => {
                        console.log("Post::", post);
                        // return (                    
                        // console.log("post:", post);
                        if (!featuredPost.id) {
                          featuredPost.id = null;
                        }
                        if (post.node.id !== featuredPost.id) {
                          return (
                            <>
                              <BlogPostCard
                                path={post.node.frontmatter.path}
                                title={post.node.frontmatter.title}
                                date={post.node.frontmatter.date}
                                cover={post.node.frontmatter.cover.childImageSharp.fluid}
                                featured={post.node.frontmatter.featured}
                                key={post.node.id}
                                author_name={post.node.frontmatter.author_name}
                              />
                              {i=== 1 && (<EmailListForm />)}
                            </>
                          )}
                        else {
                          return null;
                        }
                      })}
                    </div>
                  )
                })
                }
              
            </div>
          </div>
          {this.state.showingMore &&
            (<button
              id="more-posts-btn"
              onClick={() => {
                // this.morePosts();
                console.log("state:", this.state);
                this.setState({
                  postsToShow: this.state.postsToShow + 2,
                  showingMore: this.state.postsToShow + 2 < this.state.allPosts,
                })
              }}>
              More Posts
            </button>
            )
          }

        </div>
      </Layout>
    )
  }

}

export const pageQuery = graphql`
query indexQuery {
  normalPostsQuery: allMarkdownRemark(
    filter: {frontmatter: {published: {eq: true}}},
    sort: {order: DESC, fields: [frontmatter___date]}
  ){
    edges {
      node {
        id
        frontmatter {
          title
          path
          date
          featured
          author_name
          cover {
            childImageSharp {
              fluid(
                quality: 100,
                pngQuality: 100,
                jpegQuality: 100,
                webpQuality: 100
                ) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
  featuredPostQuery: allMarkdownRemark(
    sort: {order: DESC, fields: [frontmatter___date]},
    filter: {
      frontmatter: {
        published: {eq: true},
        featured: {eq: true}
      }
    },
    limit: 1
    ) {
    edges {
      node {
        html
        id
        frontmatter {
          path
          title
          date
          featured
          author_name
          cover {
            childImageSharp {
              fluid(
                quality: 100,
                pngQuality: 100,
                jpegQuality: 100,
                webpQuality: 100
                ) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
}
`

export default Blog