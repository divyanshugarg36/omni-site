import React from "react"
// import { Link } from "gatsby"
import Swal from 'sweetalert2'
import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"
import '../scss/style.scss'

const youtubePopup = () => {
  Swal.fire({
    showCloseButton: true,
    showConfirmButton: false,
    width: 600,
    html: '<iframe width="100%" height="300" src="https://www.youtube.com/embed/VIrZMIj4glc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
  })
}

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
      <section class="leading-content">
        <div class="content-wrapper">
        <div class="content" data-aos="fade-up">
          <h1>Fully-Decentralized Asset Platform</h1>
            <p>Omni is a platform for creating and trading custom digital assets and currencies.</p>
          <div class="action-item video-link">
              <a
                onClick={youtubePopup}
              >
            <span>Watch Video</span>
              <img alt="https://youtu.be/VIrZMIj4glc" src="images/video-preview.png"/>
            </a>
          </div>
        </div>
        </div>
      </section>

      <section id="about" class="about-content full-screen">
        <div class="content-wrapper">
          <div class="trustees">
            <p class="trust-label">We are trusted by</p>
            <div class="logo-images">
              <img alt="Bitcoin Magazine" src="images/bitcoin-magazine.svg"/>
              <img alt="Forbes" src="images/forbes.png"/>
              <img alt="Coindesk" src="images/coindesk.svg"/>
              <img alt="Yahoo" src="images/yahoo.png"/>
            </div>
            <h3>Built on top of the Bitcoin blockchain</h3>
            <p>
              Omni is a platform for creating and trading custom digital assets and currencies. 
It is a software layer built on top of the most popular, most audited, most secure blockchain -- Bitcoin. Omni transactions are Bitcoin transactions that enable next-generation features on the Bitcoin Blockchain.
            </p>
          </div>
        </div>
      </section>

      <section class="features">
        <div class="content-wrapper">
          <div class="feature feature-1">
            <div class="feature-image">
             <img alt="" src="images/rectangle_looper.svg" data-aos="fade-up"
                                                           data-aos-duration="1800"
                                                           data-aos-easing="ease-out"/>
            </div>
            <div class="feature-text">
              <h3>Easily create custom currencies</h3>
              <p>With Omni it's simple to create tokens to represent custom currencies or assets and to transact these via the Bitcoin blockchain. The power and simplicity offered by Omni has helped to make it the leading Bitcoin based token protocol.</p>
              <a href="https://www.omniexplorer.info/properties/production" rel="noopener noreferrer" target="_blank">Learn More</a>
            </div>
          </div>

          <div class="feature feature-2">
            <div class="feature-text">
              <h3>Blockchain based crowdfunding</h3>
              <p>Decentralized crowdfunding is easy with Omni. Crowdsale participants can send bitcoins or tokens directly to an issuer address and the Omni Layer automatically delivers the crowdfunded tokens to the sender in return - all without needing to trust a third party.</p>
              <a href="https://www.omniexplorer.info/crowdsales/production" rel="noopener noreferrer" target="_blank">Learn More</a>
            </div>
            <div class="feature-image">
            <img alt="" src="images/oval_looper.svg" data-aos="fade-up"
                                                           data-aos-duration="1800"
                                                           data-aos-easing="ease-out"
                                                           data-aos-anchor=".feature-2"/>
            </div>
          </div>

          <div class="feature feature-3">
            <div class="feature-image">
              <img alt="" src="images/Combined_Shape_looper.svg" data-aos="fade-up"
                                                           data-aos-duration="1800"
                                                           data-aos-easing="ease-out"/>
            </div>
            <div class="feature-text">
              <h3>Trade peer-to-peer</h3>
              <p>Participants can use the distributed exchanges provided by the Omni Layer to exchange tokens for other tokens or bitcoins directly on the blockchain without the need for a third party exchange.</p>
              <a href="https://blog.omni.foundation/2016/03/16/omnidex-getting-started/" rel="noopener noreferrer" target="_blank">Get Started</a>
            </div>
          </div>
        </div>
        
      </section>

      <section class="features-small-section">
         <div class="content-wrapper">
            <h3>Custom Currency Can Made Simple.</h3>
          <div class="right"  data-aos="fade-up">
            <ul>
              <li>
                <img alt="" src="images/icons/lock-icon.png"/>
                <span>Easy to use, secure web wallets available</span>
              </li>

              <li>
                <img alt="" src="images/icons/integrate.png"/>
                <span>Integrated with top Bitcoin and Alt-coin Exchanges</span>
              </li>

              <li>
                <img alt="" src="images/icons/bitcoin.png"/>
                <span>Easy to integrate server daemon based on Bitcoin Core</span>
              </li>

              <li>
                <img alt="" src="images/icons/screen-icon.png"/>
                <span>Fully-validating desktop wallet and client based on Bitcoin Qt</span>
              </li>

              <li>
                <img alt="" src="images/icons/tether-dollar.png"/>
                <span>Tether Dollars backed by Bank Trust, redeemable for SWIFT at tether.to and bitfinex.com</span>
              </li>

              <li>
                <img alt="" src="images/icons/dollar.png"/>
                <span>Over 1200M USD in asset market cap on the layer as of 2017</span>
              </li>
            </ul>
          </div>
         </div>
      </section>

      <section id="products" class="products-section" >
         <div class="content-wrapper">
         <div class="heading">
            <h3> Our Products </h3>
            <p>Start with a web application or a software download.</p>
          </div>
          <div class="product-listing" data-aos="fade-up">
             <div class="listing listing-1">
                 <div class="listing-name">
                     <img alt="" src="images/logo.svg"/>
                     <h4>Omni Wallet</h4>
                 </div>
                 <div class="listing-features">
                   <ul>
                     <li> Free, hosted web wallet </li>
                     <li> You control your private keys </li>
                     <li> Send and receive Bitcoin or Omni assets </li>
                     <li> Create assets, launch crowdsales, and trade on the distributed exchange </li>
                   </ul>
                 </div>
                 <a href="https://www.omniwallet.org/" rel="noopener noreferrer" target="_blank">
                 Get Started</a>
             </div>

             <div class="listing listing-2">
                 <div class="listing-name">
                     <img alt="" src="images/logo.svg"/>
                     <h4>Omni Explorer</h4>
                 </div>
                 <div class="listing-features">
                  <ul>
                    <li> Omni blockchain explorer </li>
                    <li> View Omni transactions on the Bitcoin network </li>
                    <li> Lookup Omni asset (smart property) information </li> 
                    <li> View asset trading on the distributed exchange (DEx) </li>
                  </ul>
                 </div>
                 <a href="https://omniexplorer.info/" rel="noopener noreferrer" target="_blank">
                 Visit Site</a>
             </div>

             <div class="listing listing-3">
                 <div class="listing-name">
                     <img alt="" src="images/logo.svg"/>
                     <h4>Omni Core</h4>
                 </div>
                 <div class="listing-features">
                    <ul>
                      <li> Fully-validating desktop wallet </li>
                      <li> A superset of Bitcoin Core </li>
                      <li> Mac OS X, Windows, and Linux </li>
                      <li> Native, cross-platform user interface </li>
                      <li> Peer-to-peer distributed </li>
                    </ul>
                 </div>
                 <a href="https://www.omnilayer.org/download.html"rel="noopener noreferrer" target="_blank">
                 Download</a>
             </div>
          </div>
         </div>
      </section>

      <section id="projects" class="projects-section">
          <div class="content-wrapper">
              <div class="left">
                <div class="github-branding">
                  <h3> Github Projects </h3>
                  <img alt="Github logo" src="images/github.svg"/>
                </div>
              </div>
              <div class="right" data-aos="fade-up">
              
              <a href="https://github.com/OmniLayer/spec" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">Omni Protocol Specification</p>
                  <p class="project-description">The Specification for the Omni Layer protocol.</p>
                </div>
                </a>
                <a href="https://github.com/OmniLayer/omnicore" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">Omni Core</p>
                  <p class="project-description">The Omni reference implementation. A C++ superset of Bitcoin Core.</p>
                </div>
                </a>
                <a href="https://github.com/OmniLayer/omniwallet" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">Omni Wallet</p>
                  <p class="project-description">Hosted wallet server in Python with AngularJS front-end.</p>
                </div>
                </a>
                <a href="https://github.com/OmniLayer/OmniJ" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">Omni J</p>
                  <p class="project-description">A Java/JVM Omni client.</p>
                </div>
                </a>
                <a href="https://github.com/OmniLayer/OmniTradeJS" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">Omni JS</p>
                  <p class="project-description">Node.js-based JSON-RPC client for Omni Core.</p>
                </div>
                </a>
                <a href="https://github.com/OmniLayer/" rel="noopener noreferrer" target="_blank">
                <div class="project">
                  <img alt="omni-logo" src="images/logo.svg"/>
                  <p class="project-title">See More...</p>
                  <p class="project-description">Official Omni Foundation GitHub projects.</p>
                </div>
                </a>
              </div>
          </div>
      </section>

      <section id="testimonials" class="testimonial-section">
        <div class="image">
        </div>
        <div class="content-wrapper">
          <div class="quote" data-aos="fade-up">
            <img alt="" class="start-quote" src="images/start-quotes.svg"/>
            <p>With Omni and Tether, this was probably one of the nicest integrations I’ve ever done with a blockchain asset. Having the original bitcoin RPC on top of the additional commands was exactly how it should be.</p>
            <img alt="" class="end-quote" src="images/end-quotes.svg"/>
            <div class="testimonial-writer">
              <p class="writer">Matt D</p>
              <p class="company">shapeshift.io</p>
            </div>
          </div>
        </div>
      </section>

      <section id="team" class="team-section">
        <div class="content-wrapper">
          <div class="team-heading">
          <h3> Our Team </h3>
          <p> Meet the people who are working on improving the Omni Layer. </p>
          </div>
          <div class="our-team">
            <div class='team-member'>
               <div class="photo">
                  <img alt="Adam" src="images/team/adam.png"/>
               </div>
               <p class="name">Adam</p>
               <p class="title">Founder</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Ben" src="images/team/ben.png"/>
               </div>
               <p class="name">Ben</p>
               <p class="title">Co-Founder</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Craig" src="images/team/craig.png"/>
               </div>
               <p class="name">Craig</p>
               <p class="title">Co-Founder</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="dex" src="images/team/dex.png"/>
               </div>
               <p class="name">Dexx7</p>
               {/* <p class="title">Project Manager</p> */}
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="German" src="images/team/german.png"/>
               </div>
               <p class="name">Germán</p>
               {/* <p class="title">Founder</p> */}
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Judith" src="images/team/judith.png"/>
               </div>
               <p class="name">Judith</p>
               <p class="title">Head of Recruiting</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Marv" src="images/team/marv.png"/>
               </div>
               <p class="name">Marv</p>
               <p class="title">Project Manager</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Peter" src="images/team/peter.png"/>
               </div>
               <p class="name">Peter</p>
               <p class="title">Project Lead</p>
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Sean" src="images/team/sean.png"/>
               </div>
               <p class="name">Sean</p>
               {/* <p class="title">Founder</p> */}
            </div>
            <div class='team-member'>
               <div class="photo">
                <img alt="Yann" src="images/team/yann.png"/>
               </div>
               <p class="name">Yann</p>
               {/* <p class="title">Founder</p> */}
            </div>
          </div>
         </div>
      </section>
      <footer>
        <div class="partners-wrapper">
        <div class="partners">
          <p>Our Partners</p>
          <a href="https://www.ambisafe.co/" target="_blank" rel="noopener noreferrer"><img alt="ambisafe.co" src="images/ambisafe.png"/></a>
          <a href="https://holytransaction.com/" target="_blank" rel="noopener noreferrer"><img alt="holytransation.com" src="images/holytransaction.png"/></a>
        </div>
        </div>
        <div class="social-media">
          <div class="sites">
            <p><a href="https://www.facebook.com/groups/1501816626750032/" target="_blank" rel="noopener noreferrer">Facebook</a></p>
            <p><a href="https://twitter.com/Omni_layer" target="_blank" rel="noopener noreferrer">Twitter</a></p>
            <p><a href="https://t.me/OmniLayer" target="_blank" rel="noopener noreferrer">Telegram</a></p>
            <p><a href="https://www.reddit.com/r/omni/" target="_blank" rel="noopener noreferrer">Reddit</a></p>
          </div>
          <p>© {new Date().getFullYear()} Omni Team.  All Rights Reserved.</p>
        </div>
      </footer>
  </Layout>
)

export default IndexPage
