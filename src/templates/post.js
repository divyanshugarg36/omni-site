import React from 'react';
import Layout from '../components/layout';
import SEO from "../components/seo";
import moment from 'moment';
import '../scss/post.scss'
// import Helmet from 'react-helmet';

function dateFormat(inputDate) {
    // '2020-01-21T18:30:00Z'
    let momentDate = moment(inputDate);
    if (!momentDate.isValid()) {
        momentDate = moment();
    }
    return momentDate.format('MMMM DD, YYYY');

}

export default function Template({data}) {
    const {markdownRemark: post} = data;
    return (
        <Layout>
            <SEO title={post.frontmatter.title} />
            <div class="post">
                <div class="post-header">
                    <h1 class="post-title">{post.frontmatter.title}</h1>
                    <p class="date">Posted on {dateFormat(post.frontmatter.date)}</p>
                    <p class="sub-title">{post.frontmatter.sub_title}</p>
                </div>
                <div class="post-content" dangerouslySetInnerHTML={{__html: post.html}} />
                <div class="post-author">
                    <p>Written By</p>
                    <img src={post.frontmatter.author_image.childImageSharp.fluid.src} alt="author img" />
                    <p class="author-name">{post.frontmatter.author_name}</p>
                </div>
            </div>
        </Layout>
    )
}

export const postQuery = graphql`
    query BlogPostByPath($path: String!) {
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            html
            frontmatter {
                path
                title
                sub_title
                date
                author_name
                author_image {
                    childImageSharp {
                        fluid(
                            quality: 100,
                            pngQuality: 100,
                            jpegQuality: 100,
                            webpQuality: 100
                            ) {
                            ...GatsbyImageSharpFluid
                        }
                        }
                }
            }
        }
    }
`