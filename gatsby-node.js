/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require('path');
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    const postTemplate = path.resolve('src/templates/post.js');
      // const tagPage = path.resolve('src/pages/tags.jsx');
      // const tagPosts = path.resolve('src/templates/tag.jsx');
// sort: { order: ASC, fields: [frontmatter___date] }
    resolve(
      graphql(`{
        allMarkdownRemark (sort: { fields: [frontmatter___date], order: DESC }) {
              edges {
                node {
                  html
                  id
                  frontmatter {
                    path
                    title
                  }
                }
              }
            }
      }`)
      .then(result => {
        if (result.errors) {
          return reject(result.errors);
        }
        console.log("result:", result);
        const posts = result.data.allMarkdownRemark.edges;

        posts.forEach( ({node}) => {
          createPage({
            path: node.frontmatter.path,
            component: postTemplate
          })
        })
        /*
        const postsByTag = {};
        // create tags page
        posts.forEach(({ node }) => {
          if (node.frontmatter.tags) {
            node.frontmatter.tags.forEach(tag => {
              if (!postsByTag[tag]) {
                postsByTag[tag] = [];
              }

              postsByTag[tag].push(node);
            });
          }
        });

        const tags = Object.keys(postsByTag);

        createPage({
          path: '/tags',
          component: tagPage,
          context: {
            tags: tags.sort(),
          },
        });

        //create tags
        tags.forEach(tagName => {
          const posts = postsByTag[tagName];

          createPage({
            path: `/tags/${tagName}`,
            component: tagPosts,
            context: {
              posts,
              tagName,
            },
          });
        });
        */
        //create posts
        // posts.forEach(({ node }, index) => {
        //   const path = node.frontmatter.path;
        //   const prev = index === 0 ? null : posts[index - 1].node;
        //   const next =
        //     index === posts.length - 1 ? null : posts[index + 1].node;
        //   createPage({
        //     path,
        //     component: postTemplate,
        //     context: {
        //       pathSlug: path,
        //       prev,
        //       next,
        //     },
        //   });
        // });
      })
    );
  });
};

/* Allows named imports */
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    // console.log("rss node:", node);
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

// exports.onCreateNode = ({ node, actions, getNode }) => {
//   const { createNodeField } = actions;
//   if (node.internal.type === `MarkdownRemark`) {
//     console.log("node:", node);
//     // const value = createFilePath({ node, getNode })
//     createNodeField({
//       name: `author`,
//       node,
//       value: node.frontmatter.author,
//     })
//   }
// }