module.exports = {
  siteMetadata: {
    title: `Omni`,
    description: `Omni is a platform for creating and trading custom digital assets and currencies.`,
    author: `@omni`,
    logo: `${__dirname}/static/images/logo.svg`,
    siteUrl: `https://omni-site.netlify.com`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'posts',
        path: `${__dirname}/content/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
        options: {
          name: 'images',
            path: `${__dirname}/static/images`,
        },
    },
    {
      resolve: `gatsby-source-filesystem`,
        options: {
          name: 'uploads',
            path: `${__dirname}/static/uploads`,
        },
    },

    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
        stripMetadata: true,
        defaultQuality: 100,
      },
    },
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/images/logo.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint: 'https://ryaz.us4.list-manage.com/subscribe/post?u=c5c5e2cecb3674ade8051aa0c&amp;id=155fe3f1c5',
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
