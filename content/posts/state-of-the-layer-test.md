---
cover: "../../static/uploads/1.png"
featured: false
author_name: Craig Sellers
author_image: "../../static/uploads/author.png"
date: 2020-01-30T15:19:59Z
published: true
title: Test Post 3
sub_title: 'Lots of news to share in this year-end update!  This update includes the
  following sections:'
path: "/blogpost7"

---
# Omniwallet User Experience Refresh

The talented team at Rana Dev ([https://ranadev.io](https://ranadev.io "https://ranadev.io")) has been engaged to create a new, updated version of omniwallet.org. The Omni team asked them to focus on usability and streamlining the most used aspects of the wallet, and to make the wallet work on mobile web. Initial prototypes are looking amazing and the team is pleased to share some examples of the new interface below:

![](http://localhost:8000/uploads/layer-3.png)

# Omniwallet-Mobile

We have also begun working on mobile versions of OmniWallet, testing binaries are available in our repo. OmniWallet-mobile is a native wallet for Omni Layer assets, released on both Android and iOS. It currently supports Bitcoin and all Omni Layer tokens, providing Bitcoin & Omni Layer users with an ease-of-use and unrivaled blend of security.

![](http://localhost:8000/uploads/layer-5.png)

It runs as an independent process connecting to a full node of Omni Core (version 0.7.0), which provides the on-chain services for Omni transactions on the Bitcoin network. The current OBD implementation is tightly-bound to Omni Core. Also currently being released is an API that enables light clients to communicate with OBD network. It is an easy way for our community to develop Lightning applications.